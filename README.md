## GUIDELINES

Imagines should be categorized into 3 folders, G, PG, and PG-13.  Please conservatively rate these.  Place them in the least appropriate folder when there is confusion.

Please have permission from the artists to post.  All imagine will be released under the GPL license.

No inappropriate content.  All submissions should comply with US law.
